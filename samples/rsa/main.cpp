#include <iostream>
#include "jwt/jwt.h"
#include "mcheck.h"

using namespace std;

int main() {
  jwt::error ret;

  cout << "rsa private key file: ";
  string path; cin >> path;
  jwt::buffer key = jwt::Slurp(path);

  cout << string(key.begin(), key.end()) << endl;

  cout << "rsa public key file: ";
  cin >> path;
  jwt::buffer pubkey = jwt::Slurp(path);

  cout << string(pubkey.begin(), pubkey.end()) << endl;

  jwt::builder b;
  b.set_alg("RS256");
  b.get_body()["name"] = "hello simple-jwt";
  jwt::token tok = b.build();

  if(!(ret = tok.sign(key)).ok()) {
    ret.dump(cout);
    return 1;
  }

  cout << tok.to_string() << endl;

  if(!(ret = tok.verify(pubkey)).ok()) {
    ret.dump(cout);
    return 1;
  }

  cout << "verification successful." << endl;

  return 0;
}
