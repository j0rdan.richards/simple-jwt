#include <openenclave/host.h>
#include <iostream>
#include <fstream>

#include "jwt_u.h"

using namespace std;

int error(const char* func, oe_result_t res) {
  cerr << "call to " << func << " failed: result=" << res
       << " (" << oe_result_str(res) << ")" << endl;
  return 1;
}

int error(const char* func, int res) {
  cerr << "call to " << func << " failed: result=" << res << endl;
  return 1;
}

int main(int argc, char** argv)
{
    oe_result_t result;
    oe_enclave_t* enclave = NULL;
    int ret;

    // too lazy to make a proper wrapper
    // don't want to use goto statements
    // and the enclave still has to be
    // free'd on death
    struct on_death {
      oe_enclave_t*& enclave;

      on_death(oe_enclave_t*& enclave): enclave(enclave) {}

      ~on_death() {
        if(enclave)
          oe_terminate_enclave(enclave);
      }
    } d(enclave);

    // Create the enclave
    if((result = oe_create_jwt_enclave(argv[1], OE_ENCLAVE_TYPE_AUTO,
                                       OE_ENCLAVE_FLAG_DEBUG, NULL, 0, &enclave))
       != OE_OK) {
      return error("oe_create_jwt_enclave", result);
    }

    cout << "jwt: ";
    string s; cin >> s;

    // load our jwt in the enclave
    int handle;
    if((result = enclave_load_jwt(enclave, &handle, s.c_str())) != OE_OK) {
      return error("enclave_load_jwt", result);
    }

    // get a dump for debugging purposes
    if((result = enclave_dump_jwt(enclave, &ret, handle)) != OE_OK) {
      return error("enclave_load_jwt", result);
    } else if(ret != 0) {
      return error("enclave_load_jwt", ret);
    }

    cout << "key ('__file__ <filename>' to load from a file): ";
    string key; cin >> key;
    if(key == "__file__") {
      string path; cin >> path;
      buffer key_ = Slurp(path);
      key = string(key_.begin(), key_.end());
      cout << key << endl;
    }

    // verify our jwt's signature
    if((result = enclave_verify_jwt(enclave, &ret, handle, key.c_str())) != OE_OK) {
      return error("enclave_verify_jwt", result);
    } else if(ret != 0) {
      return error("enclave_verify_jwt", ret);
    }

    cout << "key verification successful" << endl;

    return 0;
}
