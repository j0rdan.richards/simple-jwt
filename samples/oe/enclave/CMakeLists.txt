# Use the edger8r to generate C bindings from the EDL file.
add_custom_command(OUTPUT jwt_t.h jwt_t.c jwt_args.h
  DEPENDS ${CMAKE_SOURCE_DIR}/jwt.edl
  COMMAND openenclave::oeedger8r --trusted ${CMAKE_SOURCE_DIR}/jwt.edl)

add_executable(enclave enc.cpp ${CMAKE_CURRENT_BINARY_DIR}/jwt_t.c)

target_compile_definitions(enclave PUBLIC OE_API_VERSION=2)

# Need for the generated file jwt_t.h
target_include_directories(enclave PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

# Need for jwt
target_include_directories(enclave PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../../include)
target_include_directories(enclave PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../../third_party/json/include)

target_link_libraries(enclave openenclave::oeenclave openenclave::oelibcxx)
