#include <iostream>
#include <map>
#include "jwt_t.h"

#include "jwt/jwt.h"

using namespace std;

int next_id = 0;
map<int, jwt::token> tok;

int enclave_load_jwt(const char* str) {
  int id = next_id++;
  tok[id] = jwt::token(string(str));
  return id;
}

int enclave_dump_jwt(int handle) {
  if(!tok.count(handle))
    return -1;

  cout << "[enclave] header: " << tok[handle].get_parsed().header << endl;
  cout << "[enclave] body: " << tok[handle].get_parsed().body << endl;

  return 0;
}

int enclave_verify_jwt(int handle, const char* key_) {
  if(!tok.count(handle))
    return -1;

  string key(key_);

  // append a /0
  key.resize(key.size() + 1); key[key.size() - 1] = 0;

  jwt::error ret = tok[handle].verify(jwt::buffer(&key[0], key.size())).code;
  if(!ret.ok()) {
    ret.dump(cerr);
  }
  return ret.code;
}
