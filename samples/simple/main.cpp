#include <iostream>
#include <fstream>

#include "jwt/jwt.h"

using namespace std;

int main() {
  string s; cin >> s;

  jwt::token tok(s);

  cout << tok.get_header() << endl;
  cout << tok.get_body() << endl;

  cout << tok.get_parsed().header["alg"] << endl;

  jwt::buffer key_;
  string key; cin >> key;
  if(key == "__file__") {
    string path; cin >> path;
    key_ = jwt::Slurp(path);
  } else {
    key_ = jwt::buffer::alloc(key.size());
    auto it = copy(key.begin(),
                   key.end(),
                   key_.begin<uint8_t>());
  }

  cout << "key: " << jwt::base64f::encode_string(key_.buf, key_.len) << endl;

  jwt::error ret;
  if((ret = tok.verify(key_)).ok()) {
    cout << "verification successful" << endl;
  } else {
    ret.dump(cout);
    return 1;
  }

  // let's make another token
  jwt::builder b;
  b.set_alg("HS256");

  b.get_body()["name"] = "test";

  auto tok2 = b.build();

  tok2.sign(key_);

  cout << tok2.to_string() << endl;

  return 0;
}
