A simple, header-only library for reading/parsing/verifying JWT tokens.
Currently only supports HS256 and RS256, but adding new algorithms isn't too hard.
Uses mbedtls by default, but supports custom crypto implementations.


## Building?

It's header only. Simply add `include/jwt` and `third_party/json/include` to your include path and you're on your way.


## Simple Example?

Make a JWT token object from a string:
```C++
string s = // .... read the jwt in compact form somehow ....
jwt::token tok(s);
```

You can now access the header and body [json](https://github.com/nlohmann/json) objects, for instance:
```C++
auto alg = tok.get_parsed().header["alg"];
auto name = tok.get_parsed().body["name"];
```

Verification is simple. Just pass the key to the `jwt::token::verify` function (a 256-bit secret for HS256, or a PEM/DER format public key for RS256).
```C++
std::string key = "your-secret";

if(tok.verify(jwt::buffer(&key[0], key.size()).ok()) {
  std::cout << "verification successful" << std::endl;
}
```
Note: currently PEM/DER files must be a null-terminated string, so you might have to append a 0.

Creating a token uses the `jwt::builder`:
```C++
jwt::builder b;
b.set_alg("HS256");
b.get_body()["name"] = "hello simple-jwt";
jwt::token = b.build();

string key = "your-secret";
b.sign(jwt::buffer(&key[0], key.size()));
```

Full example in [samples](samples), including an [OpenEnclave example](samples/oe)

## What's Next
- Support for more algorithms
- Better error handling for those few functions that throw exceptions
