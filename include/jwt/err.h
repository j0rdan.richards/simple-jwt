#ifndef __JWT_ERROR_H_
#define __JWT_ERROR_H_

#include <vector>
#include <sstream>

// FIXME: for some reason this doesn't work from within OpenEnclave
// the msg and appended error aren't being printed
namespace jwt {
  struct error {
    int code;
    const char* msg;
    std::vector<error> next;

    error(int code = 0, const char* msg = ""): code(code), msg(msg) {}

    error& append(const error& o) { next.push_back(o); return *this; }

    // convert just this error (not children) to string
    std::string to_string() {
      std::ostringstream ss;
      ss << msg << ": (errnum=" << code << ")";
      return ss.str();
    }

    void dump(std::ostream& os) {
      os << to_string() << std::endl;

      if(next.size()) {
        os << "Caused by: ";
        next.back().dump(os);
      }
    }

    bool ok() { return code == 0; }
  };
}

#define STRINGIFY(X) #X
#define TO_STRING(X) STRINGIFY(X)
#define JWT_ERROR(code, msg) error(code, __FILE__ " @ line " TO_STRING(__LINE__) ": " msg)
#define JWT_OK error(0)

#endif // __JWT_ERROR_H_
