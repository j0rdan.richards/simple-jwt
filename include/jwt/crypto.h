#ifndef __JWT_CRYPTO_H__
#define __JWT_CRYPTO_H__

#include <vector>
#include <cstdint>
#include <string>

namespace jwt {
  enum class Algo {
    UNKNOWN,
    NONE,
    HS256,
    RS256,
  };

  Algo AlgoFromName(std::string name) {
    if(name == "HS256") return Algo::HS256;
    if(name == "RS256") return Algo::RS256;
    if(name == "NONE") return Algo::NONE;
    return Algo::UNKNOWN;
  }

  const char* NameForAlgo(Algo alg) {
    if(alg == Algo::HS256) return "HS256";
    if(alg == Algo::RS256) return "RS256";
    if(alg == Algo::NONE) return "NONE";
    return "";
  }
}

#include "compat.h"


#endif // __JWT_CRYPTO_H__
