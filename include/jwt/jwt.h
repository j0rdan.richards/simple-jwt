#include <vector>
#include <cstdint>
#include <exception>

#include "utils.h"
#include "err.h"
#include "crypto.h"
#include "b64.h"

#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace jwt {
  struct token {
    std::string header;
    std::string body;
    std::string raw;
    std::vector<uint8_t> sig;

    struct Parsed {
      bool init;
      json header;
      json body;

      Parsed(): init(false) { }
    } parsed;

    token() {}

    token(std::string header, std::string body, std::vector<uint8_t> sig = {})
      : header(header), body(body), sig(sig) {
      std::string header_b64 = base64f::encode_string(&header[0], header.size());
      std::string body_b64 = base64f::encode_string(&body[0], body.size());

      Replace(header_b64.begin(), header_b64.end(), "+/", "-_");
      Replace(body_b64.begin(), body_b64.end(), "+/", "-_");

      raw = header_b64 + std::string(".") + body_b64;
    }

    token(std::string unparsed) {
      std::vector<string_view> s = split(unparsed, '.');

      // TODO: make real exceptions
      if(s.size() != 3) throw new std::runtime_error("improper format");

      raw = std::string(s[0].len + s[1].len + 1, '.');
      {
        auto it = copy(s[0].begin(), s[0].end(), raw.begin());
        *it++ = '.';
        copy(s[1].begin(), s[1].end(), it);
      }

      header = base64f::decode(s[0]);
      body = base64f::decode(s[1]);

      std::string sig_b64(s[2].begin(), s[2].end());

      sig = base64f::decode_string(sig_b64);
    }

    std::string& get_header() { return header; }
    std::string& get_body() { return body; }
    std::string& get_raw() { return raw;  }


    void init_parsed() {
      parsed.header = json::parse(header);
      parsed.body = json::parse(body);

      parsed.init = true;
    }

    Parsed& get_parsed() {
      if(parsed.init == false) {
        init_parsed();
      }

      return parsed;
    }

    // verify the existing signature
    error verify(buffer key) {
      error ret;

      Algo algo = AlgoFromName(get_parsed().header["alg"].get<std::string>());
      if(!(ret = Crypto::Verify(algo,
                                     buffer(&get_raw()[0], get_raw().size()),
                                     key,
                                     buffer(&this->sig[0], this->sig.size()))).ok()) {
        return JWT_ERROR(2, "Signature verification failed").append(ret);
      }

      return JWT_OK;
    }

    // generate a new signature
    error sign(buffer key) {
      error ret;

      Algo algo = AlgoFromName(get_parsed().header["alg"].get<std::string>());
      buffer out;
      if(!(ret = Crypto::Sign(algo,
                              buffer(&get_raw()[0], get_raw().size()),
                              key,
                              out)).ok()) {
        return JWT_ERROR(2, "Signing failed").append(ret);
      }

      // TODO: this copy is kind of unnecessary
      sig = std::vector<uint8_t>(out.begin<uint8_t>(), out.end<uint8_t>());

      return JWT_OK;
    }

    std::string to_string() {
      std::string sig_b64 = base64f::encode_string(&sig[0], sig.size());
      Replace(sig_b64.begin(), sig_b64.end(), "+/", "-_");

      return raw + std::string(".") + sig_b64;
    }
  };

  struct builder {
    json header;
    json body;

    builder() {
      header["typ"] = "JWT";
      header["alg"] = "NONE";
    }

    json& get_header() { return header; }
    json& get_body() { return body; }

    void set_alg(const char* alg) { header["alg"] = alg; }
    void set_alg(Algo alg) { header["alg"] = NameForAlgo(alg); }

    token build() {
      return token(header.dump(), body.dump());
    }
  };
}
