#ifndef __JWT_COMPAT_H__
#define __JWT_COMPAT_H__

#include "utils.h"
#include "err.h"
#include "crypto.h"

#include "mbedtls/md.h"
#include "mbedtls/rsa.h"
#include "mbedtls/pk.h"
#include "mbedtls/x509_crt.h"

namespace jwt {
  template <typename imp>
  struct CryptoBase {
    // generate a signature
    static error Sign(Algo alg, buffer in, buffer key, buffer& out) {
      return imp::Sign(alg, in, key, out);
    }

    // verify a signature
    static error Verify(Algo alg, buffer in, buffer key, buffer sig) {
      return imp::Verify(alg, in, key, sig);
    }
  };

#ifndef CRYPTO_IMP
#define CRYPTO_IMP jwt::mbedtls_wrapper
#include "mbedtls.h"
#endif // CRYPTO_IMP

  typedef CryptoBase<CRYPTO_IMP> Crypto;
}

#endif // __JWT_COMPAT_H__
