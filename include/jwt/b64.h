// b64.h
// Author: Jordan Richards
// simple b64 encoder and decoder

#ifndef __JWT_B64_H__
#define __JWT_B64_H__

#include <cstdint>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <string>

#include "utils.h"

namespace jwt {

  // FLIP indicates whether we should switch endianness
  // before doing the conversions
  template <bool FLIP = false>
  struct Base64 {
    static int get_encoded_length(int n, bool padding = true) {
      // add an extra character here so that the number of
      // available bits is >= the number of input bits
      n *= 4;
      return (n / 3) + !!(n % 3) + (padding ? (3 - (n % 3)) % 3 : 0);
    }

    static int get_decoded_length(int n) {
      // here we knock off any extra bits we have as the input was guaranteed
      // to >= the number of required bits
      return (n * 3) / 4;
    }

    static int remove_padding(std::string& enc) {
      int padding = 0;
      int z = enc.size();
      while(enc[z-1] == '=') z--, padding++;
      enc.resize(z);
      return padding;
    }

    static int remove_padding(string_view enc) {
      int padding = 0;
      while(enc[enc.len - 1] == '=') enc.len--, padding++;
      return padding;
    }

    static bool encode(const void* _in, int len, char* buf, int buf_len) {
      const uint8_t* in = reinterpret_cast<const uint8_t*>(_in);

      int z = 0;
      for(int i = 0; i < len; i += 3) {
        uint64_t stack = 0;

        int stop = std::min(len - i, 3);
        for(int j = 0; j < stop; j++) {
          stack <<= 8; stack |= flip(in[i+stop-1-j]);
        }

        enum { mask = 0x3F };

        stop = get_encoded_length(stop, false);
        for(int j = 0; j < stop; j++) {
          if(z >= buf_len)
            return false;
          buf[z++] = b64a(flip(stack & mask, 6));
          stack >>= 6;
        }
      }

      return true;
    }

    static int decode(const char* in, int len, void* _buf, int buf_len) {
      uint8_t* buf = reinterpret_cast<uint8_t*>(_buf);

      int z = 0;
      for(int i = 0; i < len; i += 4) {
        uint64_t stack = 0;

        int stop = std::min(len - i, 4);
        for(int j = 0; j < stop; j++) {
          stack <<= 6; stack |= flip(b64f(in[i+stop-1-j]), 6);
        }

        enum { mask = 0xFF };

        stop = get_decoded_length(stop);
        for(int j = 0; j < stop; j++) {
          if(z >= buf_len)break;
          buf[z++] = flip(stack & mask);
          stack >>= 8;
        }
      }

      return z;
    }

    static std::string encode_string(void* in, int len) {
      std::string ret(get_encoded_length(len), '=');
      encode(in, len, &ret[0], ret.size());
      return ret;
    }

    static std::vector<uint8_t> decode_string(std::string in) {
      int padding = remove_padding(in);
      std::vector<uint8_t> ret(get_decoded_length(in.size()), '?');
      ret.resize(decode(&in[0], in.size(), &ret[0], ret.size()) - !!padding);
      return ret;
    }

    static std::string decode(string_view in) {
      int padding = remove_padding(in);
      std::string ret(get_decoded_length(in.len), '?');
      ret.resize(decode(&in[0], in.len, &ret[0], ret.size()) - !!padding);
      return ret;
    }

  private:
    static constexpr char b64a(int val) {
      return (val < 26) ? 'A'+val
        : (val < 52) ? 'a'-26+val
        : (val < 62) ? '0'-52+val
        : "+/"[val-62];
    }

    static constexpr int b64f(char c) {
      return (c >= 'A' && c <= 'Z') ? c-'A'
        : (c >= 'a' && c <= 'z') ? c-'a'+26
        : (c >= '0' && c <= '9') ? c-'0'+52
        : (c == '+' || c == '-') ? 62 : 63;
    }

    static uint8_t flip(uint8_t val, int n = 8) {
      if(FLIP) {
        uint8_t ret = 0;
        for(int i = 0; i < n; i++)
          ret <<= 1, ret |= val & 1, val >>= 1;
        return ret;
      } else {
        return val;
      }
    }
  };

  typedef Base64<> base64;
  typedef Base64<true> base64f;
}


#endif // __JWT_B64_H__
