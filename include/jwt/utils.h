#ifndef __JWT_UTILS_H__
#define __JWT_UTILS_H__

#include <vector>
#include <string>
#include <memory>
#include <cstring>
#include <fstream>

#include <iostream>

namespace jwt {
  struct string_view {
    char* buf;
    int len;

    string_view() {}

    string_view(char* buf, int len)
      : buf(buf), len(len) {}

    char& operator[] (int idx) { return buf[idx]; }

    char* begin() { return buf; }
    char* end() { return buf + len; }

    std::string to_string() { return std::string(begin(), end()); }
  };

  struct buffer {
    void* buf;
    int len;
    bool own;

    buffer(): buf(0), len(0), own(0) {}
    buffer(void* buf, int len, bool own = false): buf(buf), len(len), own(own) {}
    buffer(const buffer& b): buf(b.buf), len(b.len), own(false) {}
    buffer(buffer&& b): buf(b.buf), len(b.len), own(b.own) { b.own = false; }

    buffer& operator= (buffer&& b) {
      destroy();
      buf = b.buf;
      len = b.len;
      own = b.own;
      b.own = false;
      return *this;
    }

    static buffer alloc(int len) {
      if(len == 0)
        return buffer();
      return buffer(new uint8_t[len], len, true);
    }

    template <typename T = uint8_t>
    T* begin() { return reinterpret_cast<T*>(buf); }
    template <typename T = uint8_t>
    T* end() { return reinterpret_cast<T*>((size_t)buf + len); }

    bool empty() { return !buf; }

    std::string dump() {
      std::string ret(len * 2, '0');

      for(int i = 0; i < len; i++) {
        sprintf(&ret[0] + 2 * i, "%02x", reinterpret_cast<uint8_t*>(buf)[i]);
      }

      return ret;
    }

    void destroy() {
      if(own && buf) delete [] reinterpret_cast<uint8_t*>(buf);
    }

    ~buffer() {
      destroy();
    }

    inline uint8_t* data()
    { return reinterpret_cast<uint8_t*>(buf); }
    inline const uint8_t* data() const
    { return reinterpret_cast<uint8_t*>(buf); }

    bool operator== (const buffer& o) const {
      if(len != o.len) return false;
      for(int i = 0; i < len; i++)
        if(data()[i] != o.data()[i]) return false;
      return true;
    }
  };

  std::vector<string_view> split(std::string& s, char token) {
    std::vector<string_view> ret;

    int last = 0;
    for(int i = 0; i < s.size(); i++) {
      if(s[i] == token) {
        if(i != last)
          ret.push_back(string_view(&s[last], i - last));

        last = i + 1;
      }
    }

    if(s.size() != last)
      ret.push_back(string_view(&s[last], s.size() - last));

    return ret;
  }

  // in O(nm) instead of easy O(n) because I'm lazy
  // and this is used once where m = 2 anyways
  template <typename it>
  int Replace(it beg, it end, char* s, const char* e) {
    int z = 0;
    int n = strlen(s);

    while(beg != end) {
      for(int i = 0; i < n; i++)
        if(*beg == s[i]) *beg = e[i], z++;
      ++beg;
    }

    return z;
  }

  buffer Slurp(const std::string& path) {
    std::ifstream fin(path, std::ios::ate);

    int sz = fin.tellg();
    if(sz < 0) return buffer();

    buffer out = buffer::alloc(sz + 1);

    fin.seekg(std::ios::beg);

    fin.read((char*)out.buf, sz);
    out.data()[sz] = 0;

    return out;
  }
}

#endif // __JWT_UTILS_H__
