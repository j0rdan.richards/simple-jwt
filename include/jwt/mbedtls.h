#ifndef __JWT_MBEDTLS_H__
#define __JWT_MBEDTLS_H__

#include "compat.h"

namespace jwt {
  struct mbedtls_wrapper: CryptoBase<mbedtls_wrapper> {
    static error Sign(Algo alg, buffer& in, buffer& key, buffer& out) {
      switch(alg) {
      case Algo::HS256:
        {
          if(out.len < (256 / 8)) out = buffer::alloc(256 / 8);
          mbedtls_md_hmac(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                          (uint8_t*)key.buf, key.len,
                          (uint8_t*)in.buf, in.len,
                          (uint8_t*)out.buf);
          return JWT_OK;
        }

      case Algo::RS256:
        {
          int ret;
          if(out.len < 256) out = buffer::alloc(256);

          // get the hash
          buffer hash = buffer::alloc(256 / 8);
          if((ret = mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                               (uint8_t*)in.buf, in.len, (uint8_t*)hash.buf)) != 0)
            return JWT_ERROR(ret, "mbedtls error while generating hash");

          mbedtls_pk_context ctx;
          mbedtls_pk_init(&ctx);
          if((ret = mbedtls_pk_parse_key(&ctx, (uint8_t*)key.buf, key.len, 0, 0)) != 0)
            return JWT_ERROR(ret, "mbedtls error while parsing private key");

          if((ret = mbedtls_rsa_rsassa_pkcs1_v15_sign(mbedtls_pk_rsa(ctx), 0, 0,
                                                      MBEDTLS_RSA_PRIVATE, MBEDTLS_MD_SHA256,
                                                      hash.len, (uint8_t*)hash.buf,
                                                      (uint8_t*)out.buf)) != 0) {
            return JWT_ERROR(ret, "mbedtls error while generating signature");
          }

          return JWT_OK;
        }

      case Algo::NONE:
        {
          out = buffer::alloc(0);
          return JWT_OK;
        }

      default:
        return JWT_ERROR(1, "Unknown algorithm");
      }
    }

    static error Verify(Algo alg, buffer in, buffer key, buffer sig) {

      switch(alg) {
      case Algo::HS256:
        {
          error ret;
          buffer sig2;
          if(!(ret = Sign(alg, in, key, sig2)).ok())
            return JWT_ERROR(1, "Failed to sign for signature validation.")
              .append(ret);

          // just do a byte-for-byte comparison
          if(!(sig2 == sig))
            return JWT_ERROR(2, "Signature mismatch");
          return JWT_OK;
        }

      case Algo::RS256:
        {
          int ret;
          // get the hash
          buffer hash = buffer::alloc(256 / 8);
          if((ret = mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
                               (uint8_t*)in.buf, in.len, (uint8_t*)hash.buf)) != 0)
            return JWT_ERROR(ret, "mbedtls error while generating hash");

          // extract the pubkey from the cert
          mbedtls_pk_context ctx;
          mbedtls_pk_init(&ctx);
          if((ret = mbedtls_pk_parse_public_key(&ctx, (uint8_t*)key.buf, key.len)) != 0)
            return JWT_ERROR(ret, "mbedtls error while parsing public key");

          if((ret = mbedtls_rsa_rsassa_pkcs1_v15_verify(mbedtls_pk_rsa(ctx), 0, 0,
                                                        MBEDTLS_RSA_PUBLIC, MBEDTLS_MD_SHA256,
                                                        hash.len, (uint8_t*)hash.buf,
                                                        (uint8_t*)sig.buf)) != 0)
            return JWT_ERROR(ret, "mbedtls error while verifying signature");
          return JWT_OK;
        }
      case Algo::NONE:
        {
          // both signatures should be empty
          if(!sig.empty())
            return JWT_ERROR(2, "Signature is non-empty with algo=none");
          return JWT_OK;
        }

      default:
        return JWT_ERROR(1, "Unknown algorithm");
      }
    }
  };
}

#endif // __JWT_MBEDTLS_H__
